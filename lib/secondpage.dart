import 'package:flutter/material.dart';
import 'package:rolexscreen/thirdpage.dart';

class Secondpage extends StatefulWidget {
  Secondpage({Key? key}) : super(key: key);

  @override
  State<Secondpage> createState() => _SecondpageState();
}

class _SecondpageState extends State<Secondpage> {
  final Shader linearGradient = LinearGradient(
    colors: [
      Color.fromRGBO(219, 199, 169, 1),
      Color.fromRGBO(170, 146, 120, 1),
    ],
  ).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  // color: Colors.tealAccent,
                  height: 70,
                  width: 70,
                  //color: Colors.tealAccent,
                  padding: EdgeInsets.only(
                    top: 40,
                  ),
                  // child: Icon(Icons., color: Colors.white),
                  child: Image.asset('assets/images/Symbol1_rm_bg.png',
                      fit: BoxFit.contain),
                ),
                Container(
                  height: 72,
                  width: 70,
                  padding: EdgeInsets.only(
                    top: 42,
                  ),
                  child: Image.asset(
                    'assets/images/Symbol2_rm_bg.png',
                    fit: BoxFit.contain,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Divider(
              color: Color.fromRGBO(219, 199, 169, 1),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              child: Text(
                'ROLEX WATCHES',
                style: TextStyle(
                  fontFamily: 'Lora',
                  //fontWeight: FontWeight.w500,
                  fontSize: 22,
                  foreground: Paint()..shader = linearGradient,
                  height: 1.5,
                ),
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Divider(
              color: Color.fromRGBO(219, 199, 169, 1),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 15),
                  child: Text(
                    'The Collection',
                    style: TextStyle(
                      // fontFamily: 'Lora',
                      fontSize: 18,
                      color: Colors.white,
                      // foreground: Paint()..shader = linearGradient,
                      height: 1.5,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 15),
                  child: Text(
                    'See all',
                    style: TextStyle(
                      //fontFamily: 'Lora',
                      fontSize: 14,
                      foreground: Paint()..shader = linearGradient,
                      height: 1.5,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Stack(
              children: [
                Container(
                  //color: Colors.amber,
                  height: 380,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: [
                      ContainerMaker(
                          'assets/images/watch2-removebg-preview.png',
                          'GMT-MASTER',
                          '52,955'),
                      ContainerMaker(
                          'assets/images/pexels-1-removebg-preview.png',
                          'METALIC',
                          '60,499'),
                      ContainerMaker(
                          'assets/images/pexels-2-removebg-preview.png',
                          'CREAME-GOLD',
                          '80,999'),
                      ContainerMaker(
                          'assets/images/pexels-3-removebg-preview.png',
                          'LUXURIOUS',
                          '84,650'),
                      ContainerMaker(
                          'assets/images/pexels-4-removebg-preview.png',
                          'ROSE-GOLD',
                          '99,999'),
                      ContainerMaker(
                          'assets/images/pexels-photo-9978714-removebg-preview.png',
                          'VINTAGE',
                          '50,449'),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 40,
            ),
            Container(
              margin: EdgeInsets.only(right: 160),
              child: Text(
                'New Watches 2022',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                ),
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Container(
              width: 340,
              height: 150,
              decoration: BoxDecoration(
                color: Colors.white10,
                borderRadius: BorderRadius.circular(3),
              ),
              child: Row(
                children: [
                  Container(
                    width: 90,
                    //color: Colors.amber,
                    child: Image.asset(
                      'assets/images/watch3_rm_bg.png',
                      fit: BoxFit.contain,
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'YATCH MASTER  42',
                        style: TextStyle(
                          fontFamily: 'Lora',
                          fontSize: 20,
                          foreground: Paint()..shader = linearGradient,
                          height: 2.8,
                        ),
                      ),
                      Text(
                        'Glowing with new brilliance',
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w300,
                          color: Colors.white,
                          height: 1.5,
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.currency_pound_outlined,
                            color: Color.fromRGBO(170, 146, 120, 1),
                          ),
                          Text(
                            '101,999',
                            style: TextStyle(
                              fontFamily: 'Lora',
                              fontSize: 23,
                              foreground: Paint()..shader = linearGradient,
                              height: 1.5,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget ContainerMaker(image, text1, text2) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return Thirdpage(image: image , name: text1, price: text2,);
        },),);
      },
      child: Container(
        height: 380,
        margin: EdgeInsets.only(left: 15 , right: 10),
        width: 230,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [
              Color.fromRGBO(240, 255, 240, 0.3),
              Color.fromRGBO(200, 200, 200, 0.2),
              Color.fromRGBO(100, 100, 100, 0.2),
            ],
          ),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Stack(
          children: [
            Column(
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Image.asset(
                    // height: 250,
                    // width: 100,
                    image,
                    fit: BoxFit.cover,
                  ),
                  margin: EdgeInsets.only(top: 20),
                  height: 260,
                  width: 180,
                  //color: Colors.amber,
                ),
                Text(
                  text1,
                  style: TextStyle(
                    fontFamily: 'Lora',
                    //fontWeight: FontWeight.w500,
                    fontSize: 22,
                    foreground: Paint()..shader = linearGradient,
                    height: 1.5,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  'The Cosmopolitan Watch',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.currency_pound_outlined,
                        color: Color.fromRGBO(219, 199, 169, 1), size: 20),
                    Text(
                      text2,
                      style: TextStyle(
                        fontFamily: 'Lora',
                        //fontWeight: FontWeight.w500,
                        fontSize: 20,
                        foreground: Paint()..shader = linearGradient,
                        height: 1.5,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Positioned(
              top: 10,
              right: 10,
              child: Icon(
                Icons.favorite,
                color: Color.fromRGBO(219, 199, 169, 1),
              ),
            )
          ],
        ),
      ),
    );
  }
}
