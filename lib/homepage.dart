import 'package:flutter/material.dart';
import 'package:rolexscreen/secondpage.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  final Shader linearGradient = LinearGradient(
    colors: [
      Color.fromRGBO(219, 199, 169, 1),
      Color.fromRGBO(170, 146, 120, 1),
    ],
  ).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Stack(
          alignment: AlignmentDirectional.topCenter,
          children: [
            Container(
              //height: 200,
              // color: Colors.cyan,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color.fromRGBO(0, 0, 0, 1),
                      Color.fromRGBO(0, 0, 0, 1),
                      Color.fromRGBO(0, 0, 0, 0.8),
                      Color.fromRGBO(0, 0, 0, 1),
                      Color.fromRGBO(0, 0, 0, 1),
                    ]),
              ),
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  // ContainerMaker(Colors.black),
                  // ContainerMaker(Colors.black),
                  // ContainerMaker(Colors.black),
                  // ContainerMaker(Colors.black),
                  // ContainerMaker(Colors.black),
                  // ContainerMaker(Colors.black),
                  // ContainerMaker(Colors.black),
                ],
              ),
            ),
            Center(
              child: Image.asset(
                'assets/images/Watch1-removebg.png',
                fit: BoxFit.cover,
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 100,
                  height: 115,
                   //color: Colors.amber,
                  margin: EdgeInsets.only(top: 40),
                  child: Image.asset(
                      'assets/images/Rolexlogo (2).png',
                      fit: BoxFit.contain),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 20, bottom: 30),
                      child: Text(
                        'SWISS\nLUXURY\nWATCHES',
                        style: TextStyle(
                          fontSize: 25,
                          // color: Colors.white,
                          fontFamily: 'Lora',
                          foreground: Paint()
                            ..shader = linearGradient,
                          height: 1.5,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (
                            context) {
                          return Secondpage();
                        },));
                      },
                      child: Container(
                        height: 65,
                        width: 75,
                        margin: EdgeInsets.only(right: 20, top: 28),
                        // color: Colors.cyan,
                        child: Image.asset(
                            'assets/images/Arrow1-removebg-preview.png',
                            fit: BoxFit.contain),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget ContainerMaker(color,) {
    return Container(
      width: 80,
      color: color,
      margin: EdgeInsets.only(right: 20.0, left: 10.0),
    );
  }
}
