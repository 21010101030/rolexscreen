import 'package:flutter/material.dart';

class Thirdpage extends StatefulWidget {
  dynamic image;
  dynamic name;
  dynamic price;
  Thirdpage({Key? key , this.image , this.name , this.price}) : super(key: key);

  @override
  State<Thirdpage> createState() => _ThirdpageState();
}

class _ThirdpageState extends State<Thirdpage> {
  bool isFav = false;

  final Shader linearGradient = LinearGradient(
    colors: [
      Color.fromRGBO(219, 199, 169, 1),
      Color.fromRGBO(170, 146, 120, 1),
    ],
  ).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    margin: const EdgeInsets.only(top: 40, left: 10),
                    height: 40,
                    width: 50,
                    //color: Colors.tealAccent,
                    child: Transform.rotate(
                      angle: 3.14,
                      child: Image.asset(
                        'assets/images/Arrow1-removebg-preview.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 40, right: 10),
                  width: 50,
                  height: 50,
                  // color: Colors.amber,
                  child: InkWell(
                    onTap: () {
                      setState(
                        () {
                          isFav = !isFav;
                        },
                      );
                    },
                    child: Icon(
                      isFav ? Icons.favorite : Icons.favorite_border,
                      color: Color.fromRGBO(219, 199, 169, 1),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              height: 420,
              // width: 200,
              //color: Colors.tealAccent,
              child: Image.asset(widget.image.toString(),
                  fit: BoxFit.contain),
            ),
            SizedBox(
              height: 25,
            ),
            Container(
              height: 250,
              width: 330,
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color.fromRGBO(240, 255, 240, 0.3),
                    Color.fromRGBO(200, 200, 200, 0.2),
                    Color.fromRGBO(100, 100, 100, 0.2),
                  ],
                ),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        widget.name.toString(),
                        style: TextStyle(
                          fontFamily: 'Lora',
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          foreground: Paint()..shader = linearGradient,
                          height: 1.5,
                        ),
                      ),
                      Text(
                        'since 1982',
                        style: TextStyle(
                          fontFamily: 'Lora',
                          fontSize: 15,
                          foreground: Paint()..shader = linearGradient,
                          height: 1.5,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'The Cosmopolitan Watch',
                    style: TextStyle(
                      color: Colors.white70,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'An original version of the GMT-MASTER II with the \n crown on the left side of the case and a green \nand black bezel ',
                    style: TextStyle(
                      foreground: Paint()..shader = linearGradient,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'More Details',
                    style: TextStyle(
                      fontFamily: 'Lora',
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                      color: Color.fromRGBO(170, 146, 120, 1),
                      //foreground: Paint()..shader = linearGradient,
                      //height: 1.5,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        //color: Colors.amber,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '  Price',
                              style: TextStyle(
                                color: Colors.white70,
                                fontWeight: FontWeight.w600
                              ),
                            ),
                            Row(
                              children: [
                                Icon(
                                  Icons.currency_pound_outlined,
                                  color: Color.fromRGBO(170, 146, 120, 1),
                                  size: 20,
                                ),
                                Text(
                                  widget.price.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Lora',
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    foreground: Paint()
                                      ..shader = linearGradient,
                                    height: 1.5,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        color: Color.fromRGBO(170, 146, 120, 1),
                        height: 40,
                        width: 130,
                        child: Center(
                          child: Text(
                            'BUY NOW',
                            style: TextStyle(
                              fontFamily: 'Lora',
                              fontSize: 15,
                              color: Colors.black54,
                              fontWeight: FontWeight.bold,
                              // foreground: Paint()
                              //   ..shader = linearGradient,
                              height: 1.5,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
